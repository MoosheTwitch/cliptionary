const twitch = window.Twitch.ext;


const requests = {
	get: createRequest('GET', '/get/data'),
	getClip: createRequest("GET", "/get/clip")
};

function sendRequest(type, cb, data={}) {
	var obj = requests[type];
	var url = obj.url;
	var keys = Object.keys(data);
	var query = "?";
	for(var key of keys) {
		query += encodeURIComponent(key)+"="+encodeURIComponent(data[key])+"&";
	}

	if(keys.length > 0) {
		url += query.substring(0, query.length - 1);
	}
	console.log("Sending this boy "+url);

	$.ajax({
		"type": obj.type,
		"url": url,
		"success": cb,
		"error": obj.error,
		"headers": obj.headers || {}
	});
}

function createRequest(type, method, data={}) {
	var obj = {
		"type": type,
		"url": location.protocol + "//localhost:8081" + method,
		"error": function(_, err, status) {
			twitch.rig.log("Request returned "+status+" ("+error+")");
		}
	};
	return obj;
}

function setAuth(token) {
	Object.keys(requests).forEach((req) => {
		twitch.rig.log("Setting Auth Token");
		requests[req].headers = {"Authorization": "Bearer "+token};
	});
}

twitch.onContext(function(ctx) {
	twitch.rig.log(ctx);
});

twitch.onAuthorized(function(auth) {
	token = auth.token;
	uid = auth.userId;

	App.init(auth.token, auth.userId);
});

var App = new function() {

	var token;
	var uid;
	var content = {};

	function init(token, uid) {
		this.token = token;
		this.uid = uid;

		setAuth(token);

		$('.tool-sort').on("click", function() {
			var offset = $(this).offset();
			var target = "#"+$(this).data("dropdown");
			if($(target).css("display") != "none") {
				$(target).hide();
				return;
			}

			offset.top += $(this).height();
			$(target).css({
				"top": offset.top+"px",
				"left": offset.left+"px",
				"width": $(this).width()
			});
			$(target).show();
		});
		$('.dropdown-menu').on("mouseleave", function() {
			$(this).hide();
		});
		$('.dropdown-menu > *').on("click", function() {
			$("#"+$(this).parent().data("selected")).removeClass("active");
			$(this).parent().data("selected", $(this).attr("id"));
			$(this).addClass("active");
			$(this).parent().hide();
			$("#"+$(this).parent().data("label")).text($(this).text());
		});
		$('.bottom-left').on("hover", function() {
		})
		$('.bottom-list').on("scroll", function() {
			var x = $(this).scrollLeft();

			var isLeft = $('.bottom-left').css("display") != "none";
			if(x <= 0 && isLeft) {
				$('.bottom-left').fadeOut(250);
			} else if(!isLeft) {
				$('.bottom-left').fadeIn(250);
			}

			var isRight = $('.bottom-right').css("display") != "none";
			if(x  >= $(this)[0].scrollWidth - $(this).width() && isRight) {
				$('.bottom-right').fadeOut(250);
			} else if(!isRight) {
				$('.bottom-right').fadeIn(250);
			}
		});
		$('.start-config').on("click", function() {
			$('#main').hide();
			$('#config').show();
		});
		$('.start-main').on("click", function() {
			$('#config').hide();
			$('#main').show();
		});

		loadData();
	}

	function loadData() {
		sendRequest("get", function(content) {
			for(var clip of content.Clips) {
				loadClip(clip, function(ctx) {
					console.log(ctx);
					populate(ctx);
				});
			}
		}, {"channel": "Mooshe"});
	}

	function loadClip(clip, cb) {
		sendRequest("getClip", function(data) {
			var content = JSON.parse(data);
			cb(content);
		}, {"slug": clip});
	}

	function populate(ctx) {
		console.log(ctx);
		var parent = $('.top');
		var vid = $('<div class="vid"></div>');
		var ctn = $('<div class="vid-content"></div>');
		var tags = $('<div class="tags"></div>');
		if(ctx.Featured) {
			tags.append($('<span class="tag-featured">Featured</span>'));
		}
		if(ctx.Hot) {
			tags.append($('<span class="tag-hot">Hot!</span>'));
		}
		ctn.append(tags);

		var title = $('<div class="vid-title"></div>');
		title.text(ctx.Title);
		ctn.append(title);

		vid.css({"background-image": 'url('+ctx.Thumb+')'});

		vid.append(ctn);
		parent.append(vid);
	}

	function update(data) {

	}

	function error(data) {
		twitch.rig.log("[Error] "+data);
	}

	function log(data) {
		twitch.rig.log("[Info] "+data);
	}

	return {
		init: init,
		update: update,
		error: error,
		log: log
	}
};
