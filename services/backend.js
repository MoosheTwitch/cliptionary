const fs = require('fs');
const Hapi = require('hapi');
const path = require('path');
const Boom = require('boom');
const color = require('color');
const ext = require('commander');
const https = require('https');
const jsonwebtoken = require('jsonwebtoken');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const verboseLogging = true;
const verboseLog = verboseLogging ? console.log.bind(console) : () => { };

const STRINGS = {
	secretEnv: usingValue('secret'),
	clientIdEnv: usingValue('client-id'),
	serverStarted: 'Server running at %s',
	secretMissing: missingValue('secret', 'EXT_SECRET'),
	clientIdMissing: missingValue('client ID', 'EXT_CLIENT_ID'),
	cyclingColor: 'Cycling color for c:%s on behalf of u:%s',
	sendColor: 'Sending color %s to c:%s',
	invalidAuthHeader: 'Invalid authorization header',
	invalidJwt: 'Invalid JWT'
};

ext.
version(require('../package.json').version).
option('-s, --secret <secret>', 'Extension secret').
option('-c, --client-id <client_id>', 'Extension client ID').
parse(process.argv);

const secret = Buffer.from(getOption('secret', 'ENV_SECRET'), 'base64');
const clientId = getOption('clientId', 'ENV_CLIENT_ID');
const clipCache = {};

const serverOptions = {
	host: 'localhost',
	port: 8081,
	routes: {
		cors: {
			origin: ['*']
		}
	}
};
const serverPathRoot = path.resolve(__dirname, '..', 'conf', 'server');
if (fs.existsSync(serverPathRoot + '.crt') && fs.existsSync(serverPathRoot + '.key')) {
	serverOptions.tls = {
		// If you need a certificate, execute "npm run cert".
		cert: fs.readFileSync(serverPathRoot + '.crt'),
		key: fs.readFileSync(serverPathRoot + '.key')
	};
}
const server = new Hapi.Server(serverOptions);

(async () => {
	// Handle a viewer request to cycle the color.
	server.route({
		method: 'POST',
		path: '/set/',
		handler: colorCycleHandler
	});

	// Handle a new viewer requesting the color.
	server.route({
		method: 'GET',
		path: '/get/search',
		handler: searchClips
	});

	server.route({
		method: 'GET',
		path: '/get/data',
		handler: getGeneralData
	});

	server.route({
		method: "GET",
		path: "/get/clip",
		handler: getClipHandler
	})

	// Start the server.
	await server.start();
	console.log(STRINGS.serverStarted, server.info.uri);
})();

function usingValue (name) {
	return `Using environment variable for ${name}`;
}

function missingValue (name, variable) {
	const option = name.charAt(0);
	return `Extension ${name} required.\nUse argument "-${option} <${name}>" or environment variable "${variable}".`;
}

// Get options from the command line or the environment.
function getOption (optionName, environmentName) {
	const option = (() => {
		if (ext[optionName]) {
			return ext[optionName];
		} else if (process.env[environmentName]) {
			console.log(STRINGS[optionName + 'Env']);
			return process.env[environmentName];
		}
		console.log(STRINGS[optionName + 'Missing']);
		process.exit(1);
	})();
	console.log(`Using "${option}" for ${optionName}`);
	return option;
}

// Verify the header and the enclosed JWT.
function verifyAndDecode (header) {
	const bearerPrefix = "Bearer ";
	if (header.startsWith(bearerPrefix)) {
		try {
			const token = header.substring(bearerPrefix.length).trim();
			return jsonwebtoken.verify(token, secret, { algorithms: ['HS256'] });
		}
		catch (ex) {
			throw Boom.unauthorized(STRINGS.invalidJwt);
		}
	}
	throw Boom.unauthorized(STRINGS.invalidAuthHeader);
}

function colorCycleHandler (req) {
	// Verify all requests.
	const payload = verifyAndDecode(req.headers.authorization);
	const { channel_id: channelId, opaque_user_id: opaqueUserId } = payload;

	// Store the color for the channel.
	let currentColor = channelColors[channelId] || initialColor;

	// Rotate the color as if on a color wheel.
	verboseLog(STRINGS.cyclingColor, channelId, opaqueUserId);
	currentColor = color(currentColor).rotate(colorWheelRotation).hex();

	// Save the new color for the channel.
	channelColors[channelId] = currentColor;

	return currentColor;
}

function colorQueryHandler (req) {
	// Verify all requests.
	const payload = verifyAndDecode(req.headers.authorization);

	// Get the color for the channel from the payload and return it.
	const { channel_id: channelId, opaque_user_id: opaqueUserId } = payload;
	const currentColor = color(channelColors[channelId] || initialColor).hex();
	verboseLog(STRINGS.sendColor, currentColor, opaqueUserId);
	return currentColor;
}

function getGeneralData(req) {
	const payload = verifyAndDecode(req.headers.authorization);
	const { channel_id: channelId, opaque_user_id: opaqueUserId } = payload;

	var channel = req.query.channel;
	if(!fs.existsSync(serverPathRoot+"/data/"+channel+".json")) {
		console.log("file "+channel+".json does not exist");
		return {};
	}
	return new Promise(resolve => {
		fs.readFile(serverPathRoot + "/data/"+channel+".json", "utf8", function(err, data) {
			console.log(data);
			resolve(JSON.parse(data));
		});
	});
}

function searchClips(req) {
	const payload = verifyAndDecode(req.headers.authorization);
	const {
		channel_id: channelId, opaque_user_id: opaqueUserId
	} = payload;

	var channel = req.query.channel;
	var search = req.query.search;
	if(search.length > 32) {
		search = search.substring(0, 32);
	}

	if(search.startsWith("http")) {
		var split = search.split('/');
		if(split.length < 1) {
			split.push(search);
		}
		return new Promise(resolve => {
			getClipData(split[split.length - 1], function(data) {
				resolve({
					"Results": [
						data
					]
				});
			});
		});
	}
	return new Promise(resolve => {
		findClip(channel, search, function(results) {
			resove({
				"Results": results
			});
		});
	}
);
}

function findClip(channel, clipName, cb, results=[], index=0, max=5) {
	if(index >= max) {
		return results;
	}
	if(index == 0)
	clipName = clipName.toLowerCase();
	https.get("https://api.twitch.tv/kraken/clips/top?channel="+encodeURIComponent(channel)+"&trending=true&period=all&limit=100", function(res) {
		res.on("data", function(data) {
			for(var clip of data.clips) {
				clipCache[clip.slug] = getClip(clip);
				if(clip.title.toLowerCase().contains(clipName)) {
					results.push(clipCache[clip.slug]);
				}
			}
			findClip(channel, clipName, cb, results, index + 1, max);
		});
	});
}

function getClipData(clipId, cb) {
	if(clipId in clipCache) {
		cb(clipCache[clipId]);
		return;
	}

	https.get({
		"url": "https://api.twitch.tv/kraken/clips",
		"headers": {
			"Client-ID": clientId,
			"Accept": "application/vnd.twitchtv.v5+json"
		}
	}, function(res) {
		res.on("data", function(data) {
			if("slug" in data) {
				clipCache[data["slug"]] = getClip(data);
				cb(clipCache[data["slug"]]);
				return;
			}
			cb(data);
		});
	});
}

function getClip(data) {
	return {
		"Clip": data["slug"],
		"Thumb": data["thumbnails"]["medium"],
		"Title": data["title"],
		"Views": data["views"],
		"Duration": data["duration"]
	};
}

function getClipHandler(req) {
	const payload = verifyAndDecode(req.headers.authorization);
	const {
		channel_id: channelId, opaque_user_id: opaqueUserId
	} = payload;
	var slug = req.query.slug;
	return new Promise(resolve => {
		https.get({
			"url": "https://api.twitch.tv/kraken/clips/"+encodeURIComponent(slug),
			"headers": {
				"Client-ID": clientId,
				"Accept": "application/vnd.twitchtv.v5+json"
			}
		}, function(res) {
			res.on("data", function(data) {
				clipCache[data["slug"]] = getClip(data);
				resolve(clipCache[data["slug"]]);
			});
		})
	})
}
